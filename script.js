const slides = document.querySelectorAll('.slide')

for(const slide of slides){
    slide.addEventListener('mouseover',function(){
        clearActiveClasses()
        slide.classList.add('active','border')
    })  
}

function clearActiveClasses(){
    for(const slide of slides){
        slide.classList.remove('active','border')
    }
}